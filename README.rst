================
devops-playbooks
================

Various ansible playbooks and roles for various deployments.

Roles
=====
- xen_tools_ -  installs xen_tools for VMs running on xenserver
- add_to_hosts_file_ - adds ip and host name of hosts of the group to /etc/hosts on each host
- admin_users_management_ - creation/removal of users and their ssh keys
- common_admin_packages_ -roles/common_admin_pkgs/) - packages that are nice to have installed everywhere
- vim_customize_ - customize users' vim configs
- aws_manage_ - general idea for launching and configuring instances in AWS
- cdh5_repo_ - installs CDH5 repository
- cdh5_hdfs_ - installs CDH5 package of hdfs on cluster
- cdh5_yarn_ - installs CDH5 packaged yarn on cluster
- cdh5_zookeeper_ - installs CDH5 package of zookeeper
- cdh5_hive_ - installs CDH5 package of hive on cluster, hive-metastore on metastore server
- java_8_ - installs Java 8 jdk
- spark_standalone_ - installs spark_standalone on a cluster
- presto_ - installs presto on cluster

.. _xen_tools: roles/xen_tools/
.. _add_to_hosts_file: roles/add_to_hosts_file/
.. _admin_users_management: roles/admin_users_management/
.. _common_admin_packages: roles/common_admin_pkgs/
.. _vim_customize: roles/vim_customize/
.. _aws_manage: roles/aws_manage/
.. _cdh5_repo: roles/cdh5_repo/
.. _cdh5_hdfs: roles/cdh5_hdfs/
.. _cdh5_yarn: roles/cdh5_yarn/
.. _cdh5_zookeeper: roles/cdh5_zookeeper/
.. _cdh5_hive: /roles/cdh5_hive/
.. _java_8: roles/java_8/
.. _spark_standalone: roles/spark_standalone/
.. _presto: roles/presto/

Hosts config
============

Edit the hosts file and list hosts per group:
::

    [cluster]
    ab-master01
    ab-slave01
    ab-slave02
    ab-slave03
    ab-slave04
    
    # HDFS Nodes
    
    [namenodes]
    ab-master01
    
    [datanodes]
    ab-slave[01:04]
    
    # Presto Nodes
    
    [presto_coordinators]
    ab-master01
    
    [presto_workers]
    ab-slave[01:04]
    
    # Spark nodes
    
    [spark_master]
    ab-master01
    
    [spark_slaves]
    ab-slave[01:04]
    
    # Groups =========================
    
    [masters:children]
    namenodes
    presto_coordinators
    spark_master
    
    [slaves:children]
    datanodes
    presto_workers
    spark_slaves


Group Vars
==========

site_name is required in group_vars/all file. It is used in creation of hadoop config, and setting of presto ID.
E.g.:
::

    site_name: c7xen

