---
# file: roles/cdh5_hive/tasks/main.yml

- name: Install PostgreSQL server and dependencies for metastore (RedHat/CentOS)
  yum: 
     name: "{{ item }}"
     state: latest
  with_items:
    - postgresql-server
    - postgresql-jdbc
    - python-psycopg2
  when: ansible_os_family == "RedHat" and inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Install PostgreSQL server for metastore (Debian/Ubuntu)
  apt: 
     name: "{{ item }}"
     state: latest
  with_items:
    - postgresql
    - libpostgresql-jdbc-java
    - python-psycopg2
  when: ansible_os_family == "Debian" and inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Initialize postgres
  command: postgresql-setup initdb
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Configure postgres to listen on all ports
  lineinfile:
     dest: "{{ postgresql_conf }}"
     regexp: '^#listen_addresses'
     line: "listen_addresses = '*'"
     state: present
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Configure postgres turn standard_conforming_strings off
  lineinfile:
     dest: "{{ postgresql_conf }}"
     regexp: '^#standard_conforming_strings'
     line: "standard_conforming_strings = off"
     state: present
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Configure postgres to allow all ipv4 connections
  lineinfile:
     dest: "{{ postgresql_hba }}"
     regexp: '^host\s*all\s*all\s*127\.0\.0\.1/32\s*ident$'
     line: 'host    all         all         0.0.0.0         0.0.0.0               md5'
     state: present
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Start postgresql
  service:
     name: postgresql
     state: started
     enabled: yes
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Create PostgreSQL metastore db
  postgresql_db:
     name: metastore
  become: true
  become_user: postgres
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Add PostgreSQL user 'hiveuser'
  postgresql_user:
     db: metastore
     name: hiveuser
     password: "{{ site_name|lower }}_hive"
     priv: CONNECT
  become: true
  become_user: postgres
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Install hive and hive-jdbc packages on cluster
  package:
     name: "{{ item }}"
     state: latest
  with_items: 
    - hive
    - hive-jdbc
  when: inventory_hostname in groups.cluster
  tags: cdh5_hive

- name: Install hive-metastore on metastore server
  package:
     name: hive-metastore
     state: latest
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Copy /etc/hive/conf.dist to /etc/hive/conf.{{ site_name|lower }}
  command: cp -R -p /etc/hive/conf.dist "{{ hive_real_conf }}"
  when: inventory_hostname in groups.cluster
  tags: cdh5_hive

- name: Create hive alternatives conf link (/etc/hive/conf)
  alternatives:
     name: hive-conf
     link: "{{ hive_alt_conf }}"
     path: "{{ hive_real_conf }}"
  when: inventory_hostname in groups.cluster
  tags: cdh5_hive

- name: Symlink postgres jdbc jar into hive directory
  file:
     src: /usr/share/java/postgresql-jdbc.jar
     dest: /usr/lib/hive/lib/postgresql-jdbc.jar
     state: link
  when: ansible_os_family == "RedHat" and inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Template the hive-site.xml file to "{{ hive_real_conf }}"
  template:
     src: hive-site.xml
     dest: "{{ hive_real_conf }}/hive-site.xml"
     owner: root
     group: root
     mode: 0644
  when: inventory_hostname in groups.cluster
  tags: cdh5_hive

- name: Check version to know which schema to init
  shell: yum list installed hive | grep hive | awk '{print $2}' | cut -d'-' -f1 | cut -d'+' -f1
  register: version
  changed_when: False
  args:
    warn: no
  when: ansible_os_family == "RedHat" and inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Init hive metastore schema "hive-schema-{{ version.stdout }}.postgres.sql"
  command: "psql --dbname=metastore --file=/usr/lib/hive/scripts/metastore/upgrade/postgres/hive-schema-{{ version.stdout }}.postgres.sql"
  become: true
  become_user: postgres
  when: inventory_hostname in groups.hive_metastore  
  tags: cdh5_hive

- name: Grant 'hiveuser' privs to metastore db via grant-hiveuser-privs script
  script: grant-hiveuser-privs
  when: inventory_hostname in groups.hive_metastore
  tags: cdh5_hive

- name: Create a /user/hive/warehouse directory on the cluster
  command: hadoop fs -mkdir -p /user/hive/warehouse
  become: true
  become_user: hdfs
  when: inventory_hostname in groups.namenodes
  tags: cdh5_hive
    
- name: make sure the /user/hive/warehouse directory has the correct permissions
  command: hadoop fs -chmod -R 1777 /user/hive/warehouse
  become: true
  become_user: hdfs
  when: inventory_hostname in groups.namenodes
  tags: cdh5_hive

- name: Start hive-metastore service on metastore server
  service:
     name: hive-metastore
     state: started
  when: inventory_hostname in groups.namenodes
  tags: cdh5_hive
