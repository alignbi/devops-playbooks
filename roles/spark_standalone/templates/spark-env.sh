# Options for the daemons used in the standalone deploy mode
SPARK_MASTER_HOST={{ groups['spark_master'][0] }}
SPARK_MASTER_PORT={{ spark_master_port }}
SPARK_MASTER_WEBUI_PORT={{ spark_master_webui_port }}
